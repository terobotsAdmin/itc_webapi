using Chopserve_MiddleWareAPI.Data;
using Chopserve_MiddleWareAPI.Data.Interface;
using Chopserve_MiddleWareAPI.Repository.Implementation;
using Chopserve_MiddleWareAPI.Repository.Interface;
using ChopServe_MiddleWareAPI.Repository.Implementation;
using ChopServe_MiddleWareAPI.Repository.Interface;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace Chopserve_MiddleWareAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. 
            container.RegisterType<ICustomerRepository, CustomerRepository>();
            container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IDataContextFactory, DataContextFactory>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}