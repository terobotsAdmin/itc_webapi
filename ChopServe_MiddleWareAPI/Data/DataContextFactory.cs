﻿using Chopserve_MiddleWareAPI.Data.Interface;
using ITC_MiddleWareAPI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chopserve_MiddleWareAPI.Data
{
    public class DataContextFactory : IDataContextFactory
    {
        public DataContextFactory() { }

        public ChopServe_ChatbotEntities GetDataContext()
        {
            return new ChopServe_ChatbotEntities();
        }


    }
}