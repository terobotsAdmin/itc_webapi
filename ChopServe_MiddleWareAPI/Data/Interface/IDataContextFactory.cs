﻿
using ITC_MiddleWareAPI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chopserve_MiddleWareAPI.Data.Interface
{
    public interface IDataContextFactory
    {

        ChopServe_ChatbotEntities GetDataContext();
        
    }
}
