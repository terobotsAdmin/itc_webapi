﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public partial class FulfillmentMessageEntity
{
    [JsonProperty("fulfillmentMessages")]
    public FulfillmentMessageElement[] FulfillmentMessages { get; set; }
}

public partial class FulfillmentMessageElement
{
    [JsonProperty("text")]
    public Text Text { get; set; }
}

public partial class Text
{
    [JsonProperty("text")]
    public string[] TextText { get; set; }
}