﻿using Chopserve_MiddleWareAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChopServe_MiddleWareAPI.Models
{
    public class CategoryEntity
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public  List<ProductEntity> Product { get; set; }        
    }

    public class ProductEntity
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal ListPrice { get; set; }
    }

   

    public class Order
    {
        public int Id { get; set; }
        public int CustId { get; set; }

        // Navigation property
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }


    public class OrderDetail
    {
        public int CustId { get; set; }
        public decimal Quantity { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }

        // Navigation properties
        public ProductEntity Product { get; set; }
        public Order Order { get; set; }
    }

    public class DetailEntity
    {
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public decimal ListPrice { get; set; }
    }

    public class OrderDTO
    {      
        public List<DetailEntity> Details { get; set; }
        public CustomerEntity customer { get; set; }
    }

    public class ChatEntity
    {
        public string Mobile { get; set; }
        public string ChatScript { get; set; }
    }

    public class ChatLogDTO
    {
        public List<ChatEntity> ChatLogDetails { get; set; }     
    }
}