﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Google.Cloud.Dialogflow.V2;
using Google.Protobuf;
using System.IO;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using Chopserve_MiddleWareAPI.Models;
using Chopserve_MiddleWareAPI.Repository.Interface;

namespace Chopserve_MiddleWareAPI.Controllers
{



    [Route("api/webhook")]
    public class WebhookController : ApiController
    {
        private static readonly JsonParser jsonParser = new JsonParser(JsonParser.Settings.Default.WithIgnoreUnknownFields(true));

        private ICustomerRepository _customerRepository;

        #region Constructor

        public WebhookController(ICustomerRepository customerRepository)
        {

            _customerRepository = customerRepository;
        }
        #endregion

        [HttpPost]

        public async Task<IHttpActionResult> GetWebhookResponse()
        {
            WebhookRequest request;

            using (var reader = new StreamReader(await Request.Content.ReadAsStreamAsync()))
            {
                request = jsonParser.Parse<WebhookRequest>(reader);
            }

            CustomerEntity model = new CustomerEntity();

            var pas = request.QueryResult.Parameters;
            if (pas.Fields.ContainsKey("apartment") && pas.Fields["apartment"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.ApartmentName = pas.Fields["apartment"].ToString();
            else
                model.ApartmentName = String.Empty;

            if (pas.Fields.ContainsKey("address") && pas.Fields["address"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.Address = pas.Fields["address"].ToString();
            else
                model.Address = String.Empty;
          
            if(pas.Fields.ContainsKey("pincode") && pas.Fields["pincode"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.PinCode = pas.Fields["pincode"].ToString();
            else
                model.PinCode = String.Empty;


            if(pas.Fields.ContainsKey("numberofflats") && pas.Fields["numberofflats"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.NoOfFlat = pas.Fields["numberofflats"].ToString();
            else
                model.NoOfFlat = String.Empty;

           if(pas.Fields.ContainsKey("representativename") && pas.Fields["representativename"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.RepresentativeName = pas.Fields["representativename"].ToString();
            else
                model.RepresentativeName = String.Empty;


           if(pas.Fields.ContainsKey("contactnumber") && pas.Fields["contactnumber"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.Contact = pas.Fields["contactnumber"].ToString();
            else
                model.Contact = String.Empty;

           if(pas.Fields.ContainsKey("date") && pas.Fields["date"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.DateofDelivery = pas.Fields["date"].ToString();
            else
                model.DateofDelivery = String.Empty;


            if(pas.Fields.ContainsKey("timeslot") && pas.Fields["timeslot"].ToString().Replace('\"', ' ').Trim().Length > 0)
                model.TimeSlot = pas.Fields["timeslot"].ToString();
            else
                model.TimeSlot = String.Empty;

            _customerRepository.AddCustomer(model);
            WebhookResponse response = new WebhookResponse
            {
                FulfillmentText = $"Thank you for choosing our service"
            };
       
            return Json(response);
        }


    }
        
}
