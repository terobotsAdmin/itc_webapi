﻿using Chopserve_MiddleWareAPI.Models;
using Chopserve_MiddleWareAPI.Repository.Implementation;
using Chopserve_MiddleWareAPI.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Chopserve_MiddleWareAPI.Controllers
{
    [EnableCors(origins: "https://chopserveapi.azurewebsites.net", headers: "*", methods: "*")]
    public class CustomerController : ApiController
    {
        private  ICustomerRepository _customerRepository;
       
        #region Constructor

        public CustomerController(ICustomerRepository customerRepository)
        {

            _customerRepository = customerRepository;
        }
        #endregion

      [HttpPost]
        [Route("api/customer/updatestatus")]
        public bool UpdateStatus(int custId, int status)
        {
            bool result = _customerRepository.CustomerStatus(custId, status);
            return true;
        }
        
        


        [HttpGet]
        [Route("api/customer/existingcustomerdetail")]
        public CustomerEntity ExistingCustomerDetail(string mobileNum)
        {
            CustomerEntity result = _customerRepository.ExistingCustomerDetail(mobileNum);
            return result;
        }

    }
}
