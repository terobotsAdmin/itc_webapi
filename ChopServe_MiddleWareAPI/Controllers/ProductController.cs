﻿using ChopServe_MiddleWareAPI.Models;
using ChopServe_MiddleWareAPI.Repository.Interface;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ITC_MiddleWareAPI.Controllers
{

    [EnableCors(origins: "https://chopserveapi.azurewebsites.net", headers: "*", methods: "*")]
    public class ProductController : ApiController
    {
        private IProductRepository _productRepository;

        #region Constructor

        public ProductController(IProductRepository productRepository)
        {

            _productRepository = productRepository;
        }
        #endregion

        [HttpGet]
        [Route("api/product/getmenu")]
        public List<CategoryEntity> GetAllMenu()
        {
            return _productRepository.CategoryDetail();

        }

        [HttpPost]
        [Route("api/product/orderitem")]
        public bool OrderItem([FromBody]OrderDTO model)
        {
            return _productRepository.PostOrder(model);

        }

        [HttpPost]
        [Route("api/product/chatscript")]
        public bool ChatLog([FromBody]ChatLogDTO model)
        {
            return _productRepository.PostChatScript(model);

        }

        [Route("SendNotification")]
        public async Task PostMessage()
        {
            
            var client = new SendGridClient("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");
            var from = new EmailAddress("sinha.abhisek@terobots.com", "Choppy");
            List<EmailAddress> tos = new List<EmailAddress>
            {
              new EmailAddress("sinha.abhisek@gmail.com", "Abhisek"),
              new EmailAddress("42@chopserve.com", "Chopserve")
            };

            var subject = "** Alert - You have new order in queue";
            var htmlContent = "<strong>Hello, You have new order in queue. Please start the delivery.</strong>";
            var displayRecipients = false; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, "", htmlContent, false);
            var response = await client.SendEmailAsync(msg);
        }
    }

  }
