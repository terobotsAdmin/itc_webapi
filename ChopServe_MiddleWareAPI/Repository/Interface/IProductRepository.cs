﻿using ChopServe_MiddleWareAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChopServe_MiddleWareAPI.Repository.Interface
{
    public interface IProductRepository
    {
        List<CategoryEntity> CategoryDetail();
        bool PostOrder(OrderDTO orderEntity);
        bool PostChatScript(ChatLogDTO logEntity);
    }
}
