﻿using Chopserve_MiddleWareAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chopserve_MiddleWareAPI.Repository.Interface
{
    public interface ICustomerRepository
    {
        bool AddCustomer(CustomerEntity customer);

        CustomerEntity ExistingCustomerDetail(string mobileNum);
        bool CustomerStatus(int customerId, int status);

      //  string IsExistingCustomer(string mobileNum);
   //     ExistingCustomerEntity ExistingCustomerDetail(string mobileNum);
    }
}
