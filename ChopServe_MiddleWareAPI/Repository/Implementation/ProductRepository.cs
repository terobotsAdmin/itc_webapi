﻿
using Chopserve_MiddleWareAPI.Data.Interface;
using ChopServe_MiddleWareAPI.Models;
using ChopServe_MiddleWareAPI.Repository.Interface;
using ITC_MiddleWareAPI.Data;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;


namespace ChopServe_MiddleWareAPI.Repository.Implementation
{
    public class ProductRepository : IProductRepository
    {
        #region variables

        private IDataContextFactory _dataContextFactory;


        #endregion

        #region Constructor



        public ProductRepository(IDataContextFactory dataContextFactory)
        {
            _dataContextFactory = dataContextFactory;
        }

        #endregion

        public List<CategoryEntity> CategoryDetail()
        {
            
            using (ChopServe_ChatbotEntities db = _dataContextFactory.GetDataContext())
            {

                var objList = db.spSelectMenuDetails().ToList();
                var objCategory = (from m in objList group m by new { m.CategoryId, m.CategoryName} into mygroup select mygroup.FirstOrDefault()).Distinct();

                if (objList.Any())
                    {
                        var result = (from c in objCategory
                                     select new CategoryEntity()
                                     {
                                         CategoryId=c.CategoryId,
                                         CategoryName=c.CategoryName,
                                         Product= (from p in objList
                                                   where c.CategoryId==p.CategoryId
                                                   select new ProductEntity()
                                                   {
                                                      ProductId=p.ProductId,
                                                      ProductName=p.ProductName,
                                                      ListPrice=p.ListPrice
                                                   }).ToList()
                                     }).ToList();
                         return result;
                    }
                return null;
                }
            }

        public bool PostOrder(OrderDTO orderEntity)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbConn"].ToString()))
            {

         
                SqlCommand cmd = new SqlCommand("dbo.spInsertCustomerAndProductOrderItems", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar);
                cmd.Parameters["@FirstName"].Value = orderEntity.customer.FirstName;

                cmd.Parameters.Add("@LastName", SqlDbType.VarChar);
                cmd.Parameters["@LastName"].Value = orderEntity.customer.LastName;

                cmd.Parameters.Add("@Phone", SqlDbType.VarChar);
                cmd.Parameters["@Phone"].Value = orderEntity.customer.Phone;

                cmd.Parameters.Add("@Email", SqlDbType.VarChar);
                cmd.Parameters["@Email"].Value = orderEntity.customer.Email;

                cmd.Parameters.Add("@Address", SqlDbType.VarChar);
                cmd.Parameters["@Address"].Value = orderEntity.customer.Address;

                cmd.Parameters.Add("@City", SqlDbType.VarChar);
                cmd.Parameters["@City"].Value = orderEntity.customer.City;

                cmd.Parameters.Add("@State", SqlDbType.VarChar);
                cmd.Parameters["@State"].Value = orderEntity.customer.State;

                cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar);
                cmd.Parameters["@ZipCode"].Value = orderEntity.customer.ZipCode;

                cmd.Parameters.Add("@Instruction", SqlDbType.VarChar);
                cmd.Parameters["@Instruction"].Value = orderEntity.customer.Instruction;

              DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("ProductId"));
                dt.Columns.Add(new DataColumn("Quantity"));
                dt.Columns.Add(new DataColumn("ListPrice"));

                if (orderEntity.Details.Any())
                {
                   
                    foreach (var items  in orderEntity.Details)
                    {
                        DataRow workRow = dt.NewRow();
                        workRow["ProductId"] = items.ProductId;
                        workRow["Quantity"] = items.Quantity;
                        workRow["ListPrice"] = items.ListPrice;
                        dt.Rows.Add(workRow);
                    }
                }

                SqlParameter tvparam = cmd.Parameters.AddWithValue("@tblTypeOrderItems", dt);
                tvparam.SqlDbType = SqlDbType.Structured;
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    Execute();

                }
                catch (Exception ex)
                {

                }

                return true;
            }
     
        }

        public bool PostChatScript(ChatLogDTO logEntity)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbConn"].ToString()))
            {

                SqlCommand cmd = new SqlCommand("dbo.spInsertChatScript", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("Mobile"));
                dt.Columns.Add(new DataColumn("ChatScript"));
              
                if (logEntity.ChatLogDetails.Any())
                {

                    foreach (var items in logEntity.ChatLogDetails)
                    {
                        DataRow workRow = dt.NewRow();
                        workRow["Mobile"] = items.Mobile;
                        workRow["ChatScript"] = items.ChatScript;
                  
                        dt.Rows.Add(workRow);
                    }
                }

                SqlParameter tvparam = cmd.Parameters.AddWithValue("@tblTypeAuditLog", dt);
                tvparam.SqlDbType = SqlDbType.Structured;
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    return true;


                }
                catch (Exception ex)
                {

                }
                return false;
               
            }

        }

        static async Task Execute()
        {
            var client = new SendGridClient("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");
            var from = new EmailAddress("sinha.abhisek@terobots.com", "Choppy");
            List<EmailAddress> tos = new List<EmailAddress>
            {
              new EmailAddress("sinha.abhisek@gmail.com", "Abhisek"),
              new EmailAddress("42@chopserve.com", "Chopserve")
            };

            var subject = "** Alert - You have new order in queue";
            var htmlContent = "<strong>Hello, You have new order in queue. Please start the delivery.</strong>";
            var displayRecipients = false; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, "", htmlContent, false);
            var response = await client.SendEmailAsync(msg);
        }
    }
}