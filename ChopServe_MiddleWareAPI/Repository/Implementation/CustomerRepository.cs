﻿using Chopserve_MiddleWareAPI.Data;
using Chopserve_MiddleWareAPI.Data.Interface;
using Chopserve_MiddleWareAPI.Models;
using Chopserve_MiddleWareAPI.Repository.Interface;
using ITC_MiddleWareAPI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Chopserve_MiddleWareAPI.Repository.Implementation
{
    public class CustomerRepository : ICustomerRepository
    {

        #region variables

        private IDataContextFactory _dataContextFactory;


        #endregion

        #region Constructor



        public CustomerRepository(IDataContextFactory dataContextFactory)
        {
            _dataContextFactory = dataContextFactory;
        }
        
        #endregion


        #region Action

        public bool AddCustomer(CustomerEntity customer)
        {
            using (ChopServe_ChatbotEntities db = _dataContextFactory.GetDataContext())
            {
                if (customer != null)
                {
                    db.spInsertCustomerDetails(customer.FirstName, customer.LastName, customer.Phone,customer.Email, customer.Address,customer.City, customer.State, customer.ZipCode);
                    return true;
                }
            }
            return false;
        }


        public bool CustomerStatus(int customerId, int status)
        {
            using (ChopServe_ChatbotEntities db = _dataContextFactory.GetDataContext())
            {
                if (customerId >0 && status > -1)
                {
                    db.spUpdateOrderStatus(customerId, status);
                    return true;
                }
            }
            return false;

        }

        public CustomerEntity ExistingCustomerDetail(string mobileNum)
        {
            using (ChopServe_ChatbotEntities db = _dataContextFactory.GetDataContext())
            {

                if (!string.IsNullOrEmpty(mobileNum))
                {
                    var objList = db.spSelectExistingCustomerDetails(mobileNum).ToList();
                    if (objList.Any())
                    {
                       var result= from c in objList
                       select new CustomerEntity()
                        {
                            FirstName = c.FirstName,
                            LastName=c.LastName,
                            Address=c.Address,
                            Email=c.Email,
                            City=c.City ,   
                            State= c.State,
                            ZipCode=c.ZipCode                     
                        };
                        return result.FirstOrDefault();
                    }
                }
            }
            return null;
        }



        #endregion

    }
}