﻿using ITC_MiddleWareAPI.Models;
using ITC_MiddleWareAPI.Repository.Implementation;
using ITC_MiddleWareAPI.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ITC_MiddleWareAPI.Controllers
{
    [EnableCors(origins: "https://itcapi.azurewebsites.net", headers: "*", methods: "*")]
    public class CustomerController : ApiController
    {
        private  ICustomerRepository _customerRepository;
       
        #region Constructor

        public CustomerController(ICustomerRepository customerRepository)
        {

            _customerRepository = customerRepository;
        }
        #endregion

        [HttpPost]
        [Route("api/customer/addcustomer")]
        public bool AddCustomer([FromBody]CustomerEntity model)
        {
            bool result = _customerRepository.AddCustomer(model);
            return true;
        }


        [HttpGet]
        [Route("api/customer/isexistingcustomer")]
        public bool IsExistingCustomer(string mobileNum)
        {
            string result = _customerRepository.IsExistingCustomer(mobileNum);
            if (result == "TRUE")
                return true;
            else
                return false;
        }


        [HttpGet]
        [Route("api/customer/existingcustomerdetail")]
        public ExistingCustomerEntity ExistingCustomerDetail(string mobileNum)
        {
            ExistingCustomerEntity result = _customerRepository.ExistingCustomerDetail(mobileNum);
            return result;
        }

    }
}
