﻿using ITC_MiddleWareAPI.Data;
using ITC_MiddleWareAPI.Data.Interface;
using ITC_MiddleWareAPI.Models;
using ITC_MiddleWareAPI.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ITC_MiddleWareAPI.Repository.Implementation
{
    public class CustomerRepository : ICustomerRepository
    {

        #region variables

        private IDataContextFactory _dataContextFactory;


        #endregion

        #region Constructor



        public CustomerRepository(IDataContextFactory dataContextFactory)
        {
            _dataContextFactory = dataContextFactory;
        }
        
        #endregion


        #region Action

        public bool AddCustomer(CustomerEntity customer)
        {
            using (ITC_ChatBotEntities db = _dataContextFactory.GetDataContext())
            {
                if (customer != null)
                {
                    db.spInsertRepresentativeDetails(customer.RepresentativeName, customer.ApartmentName, customer.Address, customer.Contact,customer.Sender, customer.PinCode, customer.NoOfFlat, customer.DateofDelivery, customer.TimeSlot,customer.Feedback,customer.City,customer.Subscription);
                    return true;
                }
            }
            return false;
        }

        public string IsExistingCustomer(string mobileNum)
        {

           List<string> objList;
           using (ITC_ChatBotEntities db = _dataContextFactory.GetDataContext())
            {

                if (!string.IsNullOrEmpty(mobileNum))
                {
                    objList = db.spSelectExistingCustomer(mobileNum).ToList();
                    if (objList.Any())
                    {
                        string result = objList.FirstOrDefault();
                        return result;
                    }
                }
            }
            return null;
        }

       public ExistingCustomerEntity ExistingCustomerDetail(string mobileNum)
        {
            using (ITC_ChatBotEntities db = _dataContextFactory.GetDataContext())
            {

                if (!string.IsNullOrEmpty(mobileNum))
                {
                    var objList = db.spSelectExistingRepresentativeDetails(mobileNum).ToList();
                    if (objList.Any())
                    {
                       var result= from c in objList
                       select new ExistingCustomerEntity()
                        {
                            RepresentativeName = c.RepresentativeName,
                            ApartmentName=c.ApartmentName,
                            Address=c.Address,
                            PinCode=c.PinCode,
                            Contact=c.Contact ,   
                            Sender= c.Sender,
                            NoOfFlat=c.NoOfFlat,
                            City=c.City                    
                        };
                        return result.FirstOrDefault();
                    }
                }
            }
            return null;
        }

        #endregion

    }
}