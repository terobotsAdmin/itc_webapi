﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITC_MiddleWareAPI.Models
{
    public class CustomerEntity
    {
        public string RepresentativeName { get; set; }
        public string ApartmentName { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string PinCode { get; set; }
        public string NoOfFlat { get; set; }
        public string DateofDelivery { get; set; }
        public string TimeSlot { get; set; }
        public string Sender { get; set; }
        public string Feedback { get; set; }
        public string City { get; set; }
        public string Subscription { get; set; }
    }

    public class ExistingCustomerEntity
    {
        public string RepresentativeName { get; set; }
        public string ApartmentName { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string PinCode { get; set; }
        public string NoOfFlat { get; set; }
        public string Sender { get; set; }
        public string Feedback { get; set; }
        public string City { get; set; }
    }
}